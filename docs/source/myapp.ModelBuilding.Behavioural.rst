myapp.ModelBuilding.Behavioural package
=======================================

Submodules
----------

.. toctree::
   :maxdepth: 4

   myapp.ModelBuilding.Behavioural.featureengineering
   myapp.ModelBuilding.Behavioural.prepare_default
   myapp.ModelBuilding.Behavioural.prepare_limit
   myapp.ModelBuilding.Behavioural.processphonedata
   myapp.ModelBuilding.Behavioural.processtransdata
   myapp.ModelBuilding.Behavioural.required_default
   myapp.ModelBuilding.Behavioural.required_limit

Module contents
---------------

.. automodule:: myapp.ModelBuilding.Behavioural
   :members:
   :undoc-members:
   :show-inheritance:

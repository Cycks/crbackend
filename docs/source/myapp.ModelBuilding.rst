myapp.ModelBuilding package
===========================

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   myapp.ModelBuilding.Application
   myapp.ModelBuilding.Behavioural

Module contents
---------------

.. automodule:: myapp.ModelBuilding
   :members:
   :undoc-members:
   :show-inheritance:

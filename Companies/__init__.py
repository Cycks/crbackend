"""
This package deals with corporate registration



It contains the following endpoints

	1. /api/v1/register_company
	2. /api/v1/check_company
	3. /api/v1/approve_company

From the endpoints listed it is clear this module
is responsible for corporate registration, corporate
checking, and corporate approval.
"""